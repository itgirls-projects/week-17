package ru.itgirls.jdbcspringproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepositoryImplement implements BookRepository {
    @Autowired
    private DataSource dataSource;

    public BookRepositoryImplement(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Book> findAllBooks() {
        List<Book> bookList = new ArrayList<>();
        String SQL_findAllBooks = "select * from books;";

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_findAllBooks)) {
            while (resultSet.next()) {
                Book book  = convertToBook(resultSet);
                bookList.add(book);
            }
        }
        catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return bookList;
    }

    @Override
    public Book findById(Long bookId) {
        Book book = new Book();
        String SQL_findAllBooks = String.format("select * from books where id=%s;", bookId);

        try (Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_findAllBooks)) {
            while (resultSet.next()) {
                book = convertToBook(resultSet);
            }
        }
        catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return book;
    }

    private Book convertToBook(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String name = resultSet.getString("name");
        return new Book(id, name);
    }
}
