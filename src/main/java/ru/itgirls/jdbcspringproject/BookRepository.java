package ru.itgirls.jdbcspringproject;

import java.util.List;

public interface BookRepository {
    List<Book> findAllBooks();
    Book findById(Long id);
}
